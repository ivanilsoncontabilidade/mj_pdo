<?php

    try{
        $conexao = new PDO("mysql:host=localhost;dbname=pdo", "root", "");

        //$sql = "SELECT * FROM cliente";
        // foreach ($conexao->query($sql) as $dados){
        //    echo $dados['nome']."<br>";
        //}
        
        // usando PDO - prepare() => executar uma query(sql) de forma dinâmica
        $id=$_GET['id'];
        $sql = "SELECT * FROM cliente WHERE id = ?";
        $stmt = $conexao->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $linha = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        echo "<pre>";
        print_r($linha);
        echo "</pre>";
        
    } catch (PDOException $e){  // mostra um tipo de erro
                                // em caso de erro, entrar aqui
        echo "Olá! Bem vindo ao Suporte de Desenvolvimento Ivanilson Contabilidade.<br><br>";
        echo "Ocorreu o erro: " . $e->getCode() . "<br><br>";
        echo "Fale conosco (Whatsapp) (84) 99819-7040 Ivanilson, e informe-nos do erro apresentado.";
    }

    

